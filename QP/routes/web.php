<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\QuestController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\PeopleController;
use App\Http\Controllers\SettingsController;
use App\Http\Controllers\FriendshipController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Route::get('id{id}', [ProfileController::class, 'show'])->middleware('auth');
Route::get('logout', [ProfileController::class, 'logout'])->middleware('auth');

Route::get('quest/{id}/destroy', [QuestController::class, 'destroy'])->name('quest.destroy')->middleware('auth');
Route::get('quest/edit/{id}', [QuestController::class, 'edit'])->name('quest.edit');
Route::put('quest/{id}/update', [QuestController::class, 'update'])->name('quest.update')->middleware('auth');
Route::post('quest/store', [QuestController::class, 'store'])->name('quest.store')->middleware('auth');
Route::get('quest/{id}', [QuestController::class, 'open'])->name('quest.open');

Route::put('quest/{id}/addpost', [PostController::class, 'store'])->name('post.store')->middleware('auth');
Route::put('post/{id}/addcomment', [CommentController::class, 'store'])->name('comment.store')->middleware('auth');

Route::get('people', [PeopleController::class, 'show'])->middleware('auth');

Route::get('settings', [SettingsController::class, 'show'])->middleware('auth');
Route::put('settings/update', [SettingsController::class, 'update'])->name('user.update')->middleware('auth');

Route::get('id{id}/friendship', [FriendshipController::class, 'store'])->name('friendship.add')->middleware('auth');


















// Route::view('/{id}', 'livewire.profile');

// Route::middleware(['auth:sanctum', 'verified'])->get('/{id}', function () {
//     return view('livewire.profile');
// })->name('livewire.profile');
