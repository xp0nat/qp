<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ProfileController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Auth;
use App\Models\Quest;
use App\Models\Post;
use App\Models\User;
use Storage;


class QuestController extends Controller
{
    public function show($id)
    {
        //
    }

    public function destroy($id)
    {
        $quest = Quest::find($id); // id поста, который удаляем
        if ($quest->user_id == Auth::user()->id){
          //echo ($post_user_id . "=" . $user_id);
          $quest->delete();
          return redirect('/id' . Auth::user()->id)->with('success', 'Post destroyed.');
        }
        else return redirect()->back()->with('notsuccess', 'This quest does not belong to you!');
    }

    public function store(Request $request)
    {

        $request->validate([
            'title' => 'required|min:3',
            'description' => 'required|min:3',
            'questphoto' => 'file|mimes:jpg,jpeg,png,webp',
        ]);

        if (!empty($request->file('questphoto')))
        {
            $file = $request->file('questphoto')->store(Auth::user()->id ,'quests', 'quests');
        } else $file = 'default.jpg';

        Quest::create([
            'user_id' => Auth::user()->id,
            'title' => request('title'),
            'description' => request('description'),
            'picture' => $file,
        ]);
        return redirect()->back()->with('success', 'The quest has started!');
    }

    public function edit(Request $request, $id)
    {
        $quest = Quest::find($id); // id поста, который удаляем
        if ($quest->user_id == Auth::user()->id){
            $user_id = $quest->user_id;
            $user = User::where('id', $user_id)->first();
            $quests = Quest::where('user_id', $user->id)->orderBy('created_at', 'DESC')->get();
            return view('quests.edit', compact('quest', 'user', 'quests'));
        }
        else return redirect()->back()->with('notsuccess', 'This quest does not belong to you!');
    }

    public function update(Request $request, $id)
    {
        $quest = Quest::find($id);

        $request->validate([
            'title' => 'required|min:3',
            'description' => 'required|min:3',
            'questphoto' => 'file|mimes:jpg,jpeg,png,webp',
        ]);

        if (!empty($request->file('questphoto')))
        {
            $file = $request->file('questphoto')->store(Auth::user()->id ,'quests', 'quests');
        } else $file = $quest->picture;

        $quest->update([
            'title' => request('title'),
            'description' => request('description'),
            'picture' => $file,
        ]);
            
        return redirect('id' . Auth::user()->id)->with('success', 'The quest has updated!');
    }

    public function open(Request $request, $id) {
        $quest = Quest::find($id); // id поста, который открываем
        $posts = Post::where('quest_id', $quest->id)->get();
        $user = User::where('id', $quest->user_id)->first();
        return view('quests.open', compact('quest', 'posts', 'user'));
    }
}
