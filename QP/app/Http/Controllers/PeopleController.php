<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\QuestController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Auth;
use App\Models\Quest;
use App\Models\User;
use Storage;

class PeopleCOntroller extends Controller
{
    public function show()
    {
        $users = User::all();
        return view('people.show', compact('users'));
    }
}
