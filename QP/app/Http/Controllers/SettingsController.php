<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\QuestController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Auth;
use App\Models\Quest;
use App\Models\User;
use Storage;
use Carbon\Carbon;

class SettingsController extends Controller
{
    public function show()
    {
        $user = Auth::user();
        
        return view('user.settings', compact('user'));
    }

    public function update(Request $request)
    {
        $user = Auth::user();

        $request->validate([
            'name' => 'required|min:3',
            'email' => 'required|min:3',
        ]);

        $user->update([
            'name' => request('name'),
            'email' => request('email'),
        ]);
        
        return redirect()->back()->with('success', 'Your data has updated!');
    }
}
