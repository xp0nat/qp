<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\QuestController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Auth;
use App\Models\Quest;
use App\Models\User;
use App\Models\Post;
use Storage;
use Carbon\Carbon;
use App\Models\Friendship;

class ProfileController extends Controller
{
    public function show(Request $request, $id)
    {   
        
        
        if ($friendship = Friendship::where('user_1', Auth::user()->id)->first() && 'condition' == 1)
        {
            $fr = 1;
        } else $fr = 0;

        $user = User::where('id', $id)->first();
        $quests = Quest::where('user_id', $user->id)->orderBy('created_at', 'DESC')->take(5)->get();
  
        return view('user.profile', compact('quests', 'user', 'fr'));
    }
    
    public function logout()
    {
        Auth::logout();
        return redirect()->to('login');
    }
}
