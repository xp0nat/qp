<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Quest;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class Profile extends Component
{
    public $user, $name;

    public function render()
    {
        $this->user = Auth::user();
        return view('livewire.profile.index');
    }

    public function addQuest($condition)
    {
        $this->emit('addQuest', $condition);
    }
}
