<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Quest;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class Quests extends Component
{
    public $quests, $title, $description, $selected_id, $iduser, $condition, $user_id, $request;


    public function render()
    {  
        $user_id = $request->route('id');
        $this->quests = Quest::all()->where('user_id', $user_id)->sortByDesc('created_at'); //where('user_id', $user->id)->
        return view('livewire.quests');
    } 

    public function destroy($id)
    {
        if ($id)
        {
            $quest = Quest::where('id', $id);
            $quest->delete();
            $this->emit('refresh_quests');
        }
    }

    private function resetInput()
    {
        $this->title = null;
        $this->description = null;
    }

    public function store()
    {
        $this->validate([
            'title' => 'required|min:3',
            'description' => 'required|min:3',
        ]);

        Quest::create([
            'user_id' => $this->user = Auth::user()->id,
            'title' => $this->title,
            'description' => $this->description,
        ]);
        session()->flash('success', 'Quest is run!');
        $this->resetInput();

        $this->condition = 0;
    }

    public function edit($id)
    {
        $quest = Quest::findOrFail($id);
        $this->selected_id = $id;
        $this->title = $quest->title;
        $this->description = $quest->description;

        $this->condition = 2;
    }

    public function update()
    {
        $this->validate([
            'title' => 'required|min:3',
            'description' => 'required|min:3',
        ]);

        if ($this->selected_id)
        {
            $post = $quest = Quest::find($this->selected_id);

            $post->update([
                'title' => $this->title,
                'description' => $this->description,
            ]);
            
            session()->flash('success', 'Quest is updated!');
            $this->resetInput();
    
            $this->condition = 0;
        }
    }

    protected $listeners = ['addQuest' => 'incrementAddQuest'];
    public function incrementAddQuest(){
        if ($this->condition == 0)
        $this->condition = 1;
        elseif ($this->condition == 2)
            {
                $this->condition = 1;
                $this->resetInput();
            }
        elseif ($this->condition == 1)
            $this->condition = 0;
    }
}
// protected $listeners = ['addQuest' => 'incrementAddQuest'];
// public function incrementAddQuest(){
//     $this->emitTo('quests', 'addQuest');
//     if ($this->condition == 0)
//     $this->condition = 1;
//     elseif ($this->condition == 1)
//     $this->condition = 0;
//     elseif ($this->condition == 2)
//     $this->condition = 1;
// }
