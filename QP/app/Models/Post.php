<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Quest;
use App\Models\Comment;

class Post extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'quest_id',
        'content',
    ];

    /**
     *  Получить квест данного поста
     */
    public function quest(){
        return $this->belongsTo(Quest::class);
    }

    /**
    *   Получить комменты поста
    */
    public function comments(){
        return $this->hasMany(Comments::class);
    }
    public function getCommentsCountAttribute(){
        return $this->comments()->count();
    }
    public function addComment(){
        $comment = Comment::where()->('id', $id);
        dd($comment);

        $request->validate([
            'content' => 'required|min:3',
        ]);

        Comment::create([
            'user_id' => Auth::user()->id,
            'post_id' => Comment::post()->id,
            'content' => request('description'),
        ]);
        return redirect()->back()->with('success', 'OK!');
    }
}
