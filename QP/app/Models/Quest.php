<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Post;

class Quest extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'title',
        'description',
        'picture',
    ];

    /**
    *   Получить посты данного квеста
    */
    public function posts(){
        return $this->hasMany(Post::class);
    }
    public function getPostsCountAttribute(){
        return $this->posts()->count();
    }

}
