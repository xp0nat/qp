@extends('layouts.app')

@section('content')</br>
@section('title', 'People')

<!-- Вывод ошибок и оповещений -->
<div class="namespace">All people</div></br>
@include('output')
<!-- People -->
<ul class="list-group">
    @foreach ($users as $user)
        <li class="list-group-item"> <a href="/id{{$user->id}}">{{$user->name}}</a> ~ {{$user->email}}</li>
    @endforeach
</ul>


@endsection
