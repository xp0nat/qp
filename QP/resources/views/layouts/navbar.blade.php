<div class="navbar-back">
    <nav class="navbar container navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="#">QuestProject</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Home</a>
                </li>
                <li class="nav-item active">
                <a class="nav-link" href="/id{{Auth::user()->id}}">My Quests</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="#">My Friends</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="/people">People</a>
                </li>
            </ul>
        </div>

    <span class="navbar-text">
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item active">
                <a class="nav-link" href="/settings">Profile settings</span></a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="/logout">Logout</a>
                </li>
            </ul>
        </div>
    </span>

    </nav>
</div>