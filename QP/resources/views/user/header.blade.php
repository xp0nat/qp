<div class="card">
    <div class="card-body">
      <h5 class="card-title">{{$user->name}}</h5>
      <h6 class="card-subtitle mb-2 text-muted">Online</h6>
      <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>


      <a href="{{ route('friendship.add', ['id' => $user->id]) }}" class="card-link">Friendship</a>



    </div>
</div>