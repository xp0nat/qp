@extends('layouts.app')

@section('content')</br>

<!-- Profile -->
@include('user.header')
<!-- Вывод ошибок и оповещений -->
</br><div class="namespace">Profile settings</div></br>
@include('output')

<div class="card">
    <div class="card-body">
        {{ Form::model($user, array('route' => array('user.update', $user->id), 'method' => 'PUT')) }}
        <div class="form-group">
            <label for="name">name</label>
        <input name="name" type="text" class="form-control" value="{{$user->name}}">
            </div>
        <div class="form-group">
            <label for="email">email</label>
            <input name="email" type="text" class="form-control" value="{{$user->email}}">
        </div>
        
        <div class="form-group">
            <label for="exampleFormControlFile1">Example file input</label>
            <input type="file" class="form-control-file" id="exampleFormControlFile1">
        </div>
        
        <button type="submit" class="btn btn-primary">Submit</button>
        {!! Form::close() !!}
    </div>
</div>

@endsection
