<div class="">

<div class="namespace">Quests</div>

@include('layouts/output')

@if ($condition == 0)
@elseif($condition == 1)
    @include('livewire.quests.store')
@elseif($condition == 2)
    @include('livewire.quests.update')
@endif



@foreach ($quests as $quest)
<div class="row quest">
    <div class="col-md-8 col-sm-8 col-8" style="padding-right: 0px;">
        <div style="font-size: 20px;">{{$quest->title}}</div>
        <div style="font-size: 16px;">{{$quest->description}}</div>
        <div style="font-size: 13px;" class="row">
            <button wire:click="destroy({{$quest->id}})" class="btn">Delete</button>
            <button wire:click="edit({{$quest->id}})" class="btn">Update</button>
        </div>
    </div>
    <div class="col-md-4 col-sm-4 col-4" style="padding-left: 0px">
        <div class="quest-image text-right col-right">
            <div class="quest-knock">
                <div class="quest-knock-knock">
                  12
                </div>
            </div>
        </div>
    </div>
</div>
@endforeach

</div>
