<div class="row">
    <div class="col-md-2 col-sm-2 col-2 col-lg-2 col-xl-2" style="padding: 0px;">
        <div class="knock"><i class="fas fa-arrow-left"></i></div>
    </div>
    <div class="col-md-8 col-sm-8 col-8 col-lg-18">
        <div class="profile-avatar">
        <b class="namespace-name">{{$user->name}}</b>
        </div>
    </div>
    <div class="col-md-2 col-sm-2 col-2 col-lg-2 col-xl-2" style="padding: 0px;">
        <div wire:click="$emitTo('quests', 'addQuest')" class="knock"><i class="fas fa-plus"></i></div>
    </div>
</div>

