<div class="row quest">
    <div class="col-md-8 col-sm-8 col-8" style="padding-right: 0px;">
        <div><input wire:model="title" class="input" style="width: 100%;" type="text" placeholder="Title"></div>
        <div style="margin-top: 10px;"><input wire:model="description" class="input" style="width: 100%;" type="text" placeholder="Description"></div>
    </div>
    <div class="col-md-4 col-sm-4 col-4" style="padding-left: 0px">
        <div class="quest-image-add text-right col-right">
            <div class="quest-image-add-icon"><i class="fas fa-camera"></i></div>
            <div class="quest-knock">
                <div wire:click="store" class="quest-knock-knock">
                    <i class="fas fa-paper-plane"></i>
                </div>
            </div>
        </div>
    </div>
</div>
