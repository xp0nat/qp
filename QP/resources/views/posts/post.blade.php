@foreach ($posts as $post)
    <div class="card">
        <div class="card-body">
        <h5 class="card-title">{{$post->content}}</h5>
        {{-- <a href="{{ route('quest.destroy', ['id' => $quest->id]) }}" class="card-link">Destroy</a>
        <a href="{{ route('quest.edit', ['id' => $quest->id]) }}" class="card-link">Update</a>
        <a href="{{ route('quest.open', ['id' => $quest->id]) }}" class="card-link">Open the Quest</a> --}}
        <h6 class="card-subtitle mb-2 text-muted">
            
        </br><i class="fas fa-plus-square"></i> {{$quest->created_at->diffForHumans()}}   &ensp;
        
        @if ($quest->created_at != $quest->updated_at)
            <i class="fas fa-pen-square"></i> {{$quest->updated_at->diffForHumans()}}
        @endif

        </br></br>


        {{ Form::model($comment, $post, array('route' => array('comment.store', $comment->id), 'method' => 'put', 'enctype' => 'multipart/form-data')) }}
        @csrf
            <div class="form-group">
                <label for="exampleFormControlInput1">Comment</label>
                <input name="comment" type="text" class="form-control" id="exampleFormControlInput1" placeholder="type">
            </div>

                {{-- file upload --}}
            
            <button type="submit" class="btn btn-primary">Submit</button>
        {!! Form::close() !!}
            
        </h6>
        </div>
    </div>

@endforeach





