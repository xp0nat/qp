@include('output')

<div class="card">
    <div class="card-body">
        @if($user == Auth::user())
        {{ Form::model($quest, array('route' => array('post.store', $quest->id), 'method' => 'put', 'enctype' => 'multipart/form-data')) }}
            @csrf
            <div class="form-group">
                <label for="content">Write a few words</label>
                <input name="content" type="text" class="form-control">
                </div>

                {{-- file upload --}}
            
            <button type="submit" class="btn btn-primary">Submit</button>
        {!! Form::close() !!}
        
        @else
        Недостаточно прав для добавление чужих записей
        @endif
    </div>
</div>
