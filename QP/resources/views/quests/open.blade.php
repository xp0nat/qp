@extends('layouts.app')

@section('content')</br>
@section('title', 'Quest')

<!-- Profile -->

@include('user.header')

</br><div class="namespace">Quest <a href="{{ url()->previous() }}">Get back</a> </div></br>

<div class="card">
    <div class="card-body">
    <h5 class="card-title">{{$quest->title}}</h5>
    <p class="card-text">{{$quest->description}}</p>
    <a href="{{ route('quest.destroy', ['id' => $quest->id]) }}" class="card-link">Destroy</a>
    <a href="{{ route('quest.edit', ['id' => $quest->id]) }}" class="card-link">Update</a>
    <a href="{{ route('quest.open', ['id' => $quest->id]) }}" class="card-link">Open the Quest</a>
    <h6 class="card-subtitle mb-2 text-muted">
        
    </br><i class="fas fa-plus-square"></i> {{$quest->created_at->diffForHumans()}}   &ensp;
    
    @if ($quest->created_at != $quest->updated_at)
        <i class="fas fa-pen-square"></i> {{$quest->updated_at->diffForHumans()}}
    @endif

    </br></br><div style="width: 100px; height: 100px; background: black url('{{ URL::asset('storage/quests/' .$quest->picture) }}'); background-size: cover;"></div>
        
    </h6>
    </div>
</div>

</br>
@include('posts.add')</br>
@include('posts.post')

@endsection
