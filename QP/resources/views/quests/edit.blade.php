@extends('layouts.app')

@section('content')</br>

<!-- Profile -->
@include('user.header')</br>
<div class="namespace">Quests</div></br>
<!-- Вывод ошибок и оповещений -->
@include('output')
<!-- Quest add -->
@include('quests.update')</br>
<!-- Quests -->
@include('quests.quest')

@endsection
