<div class="card">
    <div class="card-body">
        @if($user == Auth::user())
        {{ Form::model($quest, array('route' => array('quest.update', $quest->id), 'method' => 'PUT', 'enctype' => 'multipart/form-data')) }}
        <div class="form-group">
            <label for="title">Title</label>
        <input name="title" type="text" class="form-control" value="{{$quest->title}}">
            </div>
        <div class="form-group">
            <label for="description">Description</label>
            <input name="description" type="text" class="form-control" value="{{$quest->description}}">
        </div>
        
        <div class="form-group">
            <label for="questphoto">Example file input</label>
            <input name="questphoto" type="file" class="form-control-file" id="exampleFormControlFile1">
        </div>
        
        <button type="submit" class="btn btn-primary">Submit</button>
        {!! Form::close() !!}
        
        @else
        Недостаточно прав для редактирования чужих записей
        @endif
    </div>
</div>