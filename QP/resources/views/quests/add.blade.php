<div class="card">
    <div class="card-body">
        @if($user == Auth::user())
        <form action="{{route('quest.store')}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="title">Title</label>
                <input name="title" type="text" class="form-control">
                </div>
            <div class="form-group">
                <label for="description">Description</label>
                <input name="description" type="text" class="form-control">
            </div>
            
            <div class="form-group">
                <label for="questphoto">Example file input</label>
                <input name="questphoto" type="file" class="form-control-file" id="exampleFormControlFile1">
            </div>
            
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
        
        @else
        Недостаточно прав для добавление чужих записей
        @endif
    </div>
</div>
